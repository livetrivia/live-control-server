package service

import "golang/entity"

type IControlService interface {
	Start(int) error
	ShowQuestion(int, int) error
	ShowAnswer(int, int) error
	ShowWinner(int, int) error
	End(int) error
}

var ControlService IControlService

type ILiveService interface {
	Create(title string, prize int, planTs string) (*entity.Live, error)
	CopyCreate(liveId int) (*entity.Live, error)
	Get(liveId int) (*entity.Live, error)
	Now() (*entity.Live, error)
	List(pageno, pagesize int) ([]*entity.Live, int, error)
	Update(liveId int, title string, prize int, planTs string) (*entity.Live, error)
}

var LiveService ILiveService
