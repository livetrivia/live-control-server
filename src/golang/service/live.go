package service

import "golang/dao"
import "golang/entity"
import "time"

type Live struct{}

func NewLive() (*Live, error) {
	return &Live{}, nil
}

func (this *Live) Create(title string, prize int, planTs string) (*entity.Live, error) {
	return dao.LiveDao.Create(title, prize, planTs)
}

func (this *Live) CopyCreate(liveId int) (*entity.Live, error) {
	//todo check question and part copy, call by cms?
	return dao.LiveDao.CopyCreate(liveId)
}

func (this *Live) Get(liveId int) (*entity.Live, error) {
	return dao.LiveDao.Get(liveId)
}

func (this *Live) Now() (*entity.Live, error) {
	live, err := dao.LiveDao.Now()
	if err != nil {
		return nil, err
	}

	tplanTs, _ := time.Parse("2006-01-02 15:04:05", live.PlanTs)
	live.Countdown = int(time.Now().Sub(tplanTs) / time.Second)
	if live.Countdown < 0 {
		live.Countdown = 0
	}
	if live.Countdown <= 10*60 && live.Status == entity.LiveStatusStart {
		live.Active = true
	}
	return live, nil
}

func (this *Live) List(pageno, pagesize int) ([]*entity.Live, int, error) {
	lives, total, err := dao.LiveDao.List(pageno, pagesize)
	for i := range lives {
		if lives[i].Status == entity.LiveStatusStart {
			lives[i].Active = true
		}
	}

	return lives, total, err
}

func (this *Live) Update(liveId int, title string, prize int, planTs string) (*entity.Live, error) {
	return dao.LiveDao.Update(liveId, title, prize, planTs)
}
