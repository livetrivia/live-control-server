package service

import (
	"encoding/json"
	"golang/dao"
	"golang/entity"
	"time"

	"git.pandatv.com/panda-public/logkit"
)

type Control struct{}

func NewControl() (*Control, error) {
	return &Control{}, nil
}

func (this *Control) Start(liveId int) error {
	live, err := this.chechLiveNow(liveId)
	if err != nil {
		return err
	}

	if live.Status != entity.LiveStatusNoStart {
		return entity.ErrorLiveStatusWrong
	}

	cq := entity.CurrentQuestion{}
	cq.LiveId = live.LiveId
	cq.LiveStatus = entity.LiveStatusStart
	err = dao.QuestionDao.SetCurrentQuestion(&cq)
	if err != nil {
		return err
	}

	err = dao.LiveDao.Start(live.LiveId)
	return err
}

func (this *Control) ShowQuestion(liveId, questionId int) error {
	live, err := this.chechLiveNow(liveId)
	if err != nil {
		return err
	}

	if live.Status != entity.LiveStatusStart {
		return entity.ErrorLiveStatusWrong
	}

	question, err := dao.QuestionDao.GetQuestionById(questionId)
	if err != nil {
		return err
	}

	if question == nil {
		return entity.ErrorQuestionNotExist
	}

	if question.Status != entity.QuestionStatusUnshow || question.Used == 1 {
		return entity.ErrorQuestionAlreadyUsed
	}

	cq, err := dao.QuestionDao.GetCurrentQuestion()
	if err != nil {
		return err
	}

	var lastQid int
	if cq != nil {
		if cq.QuestionId != questionId {
			lastQid = cq.QuestionId
			lastQuestion, err := dao.QuestionDao.GetQuestionById(lastQid)
			if err != nil {
				return err
			}

			if lastQuestion == nil {
				return entity.ErrorQuestionNotExist
			}

			if lastQuestion.AnswerShowed != 1 || lastQuestion.Status != entity.QuestionStatusOff {
				return entity.ErrorExistQuestionGoing
			}
		} else {
			return entity.ErrorQuestionAlreadyUsed
		}
	}

	nextcp := entity.CurrentQuestion{}
	nextcp.CorrectOption = question.CorrectOption
	if question.DisplayOrder == 11 {
		nextcp.IsFinal = true
	}
	nextcp.LastQuestionId = lastQid
	nextcp.LiveId = live.LiveId
	nextcp.LiveStatus = live.Status
	nextcp.QuestionId = question.QuestionId
	nextcp.QuestionStatus = entity.QuestionStatusShow
	nextcp.ShowTime = int(time.Now().Unix()) * 1000

	err = dao.QuestionDao.SetCurrentQuestion(&nextcp)
	if err != nil {
		return err
	}

	qm := entity.FormatQuestionMsg(question)
	questionJson, _ := json.Marshal(qm)
	err = dao.MsgDao.Push(string(questionJson))
	if err != nil {
		return err
	}

	err = dao.QuestionDao.SetQuestionStatus(question.QuestionId, entity.QuestionStatusShow)
	if err != nil {
		return err
	}

	go func() {
		<-time.Tick(time.Second * 25)
		err := dao.QuestionDao.SetQuestionStatus(question.QuestionId, entity.QuestionStatusOff)
		if err != nil {
			logkit.Errorf("set question %d status %d error:%s", question.QuestionId, entity.QuestionStatusOff, err)
		}
		err = dao.QuestionDao.SetCurrentQuestionStatus(question.QuestionId, entity.QuestionStatusOff)
		if err != nil {
			logkit.Errorf("set current question %d status %d error:%s", entity.QuestionStatusOff, err)
		}
	}()

	return nil
}

func (this *Control) ShowAnswer(liveId, questionId int) error {
	live, err := this.chechLiveNow(liveId)
	if err != nil {
		return err
	}

	if live.Status != entity.LiveStatusStart {
		return entity.ErrorLiveStatusWrong
	}

	cq, err := dao.QuestionDao.GetCurrentQuestion()
	if err != nil {
		return err
	}

	if cq == nil {
		return entity.ErrorQuestionNotExist
	}

	if cq.QuestionId != questionId {
		return entity.ErrorNotMatchNowQuestion
	}

	if cq.QuestionStatus != entity.QuestionStatusOff {
		return entity.ErrorExistQuestionGoing
	}

	if cq.AnswerShowed == 1 {
		return entity.ErrorQuestionAlreadyShowAnswer
	}

	question, err := dao.QuestionDao.GetQuestionById(questionId)
	if err != nil {
		return err
	}

	if question == nil {
		return entity.ErrorQuestionNotExist
	}

	stat, err := dao.AnswerDao.GetStatistic(live.LiveId, questionId)
	if err != nil {
		return err
	}

	am := entity.FormatAnswerMsg(question, stat)
	ascoJson, _ := json.Marshal(am)
	err = dao.MsgDao.Push(string(ascoJson))
	if err != nil {
		return err
	}

	cq.AnswerShowed = 1
	err = dao.QuestionDao.SetCurrentQuestion(cq)
	if err != nil {
		return err
	}

	err = dao.QuestionDao.SetQuestionShowAnswerAndUsed(questionId)

	return err
}

func (this *Control) ShowWinner(liveId, questionId int) error {
	live, err := this.chechLiveNow(liveId)
	if err != nil {
		return err
	}

	if live.Status != entity.LiveStatusStart {
		return entity.ErrorLiveStatusWrong
	}

	cq, err := dao.QuestionDao.GetCurrentQuestion()
	if err != nil {
		return err
	}

	if cq == nil {
		return entity.ErrorQuestionNotExist
	}

	if cq.QuestionId != questionId {
		return entity.ErrorNotMatchNowQuestion
	}

	if cq.QuestionStatus != entity.QuestionStatusOff {
		return entity.ErrorExistQuestionGoing
	}

	if cq.AnswerShowed != 1 {
		return entity.ErrorQuestionNoShowAnswer
	}

	winner, err := dao.AnswerDao.GetWinner(live.LiveId)
	if err != nil {
		return err
	}

	var money int
	if len(winner) == 0 {
		money = 0
	} else {
		money = int(float64(live.Prize) / float64(len(winner)))
		go func() {
			check, err := dao.RankDao.CheckInsert(live.LiveId)
			if err != nil {
				logkit.Errorf("chech insert liveId:%d error:%s", live.LiveId, err)
				return
			}
			if check {
				return
			}
			err = dao.RankDao.InsertWinner(live.LiveId, winner, money)
			if err != nil {
				logkit.Errorf("insert rank winner %s, prize %d, error:%s", winner, money, err)
			}
			err = dao.UserDao.UpdatePrize(winner, money)
			if err != nil {
				logkit.Errorf("update user %s, prize %d, error:%s", winner, money, err)
			}
		}()
	}

	var winnerInfos []*entity.UserInfo
	if len(winner) > 100 {
		winnerInfos, err = dao.UserDao.ListUser(winner[:100])
	} else {
		winnerInfos, err = dao.UserDao.ListUser(winner)
	}
	if err != nil {
		return err
	}

	winnerInfosAndPrize := make([]*entity.Winner, len(winnerInfos))
	for i, wi := range winnerInfos {
		winnerInfosAndPrize[i].Prize = money
		winnerInfosAndPrize[i].User = wi
	}

	wm := entity.FormatWinnerMsg(live.LiveId, len(winner), winnerInfosAndPrize)
	wmJson, _ := json.Marshal(wm)
	err = dao.MsgDao.Push(string(wmJson))

	return err
}

//更新榜单
func (this *Control) End(liveId int) error {
	live, err := this.chechLiveNow(liveId)
	if err != nil {
		return err
	}

	if live.Status != entity.LiveStatusStart {
		return entity.ErrorLiveStatusWrong
	}

	err = dao.QuestionDao.DelCurrentQuestion()
	if err != nil {
		return err
	}

	go func() {
		err := dao.RankDao.UpdateRank()
		if err != nil {
			logkit.Errorf("update rank error:%s", err)
		}
	}()

	em := entity.FormatEndMsg(live.LiveId)
	emJson, _ := json.Marshal(em)
	err = dao.MsgDao.Push(string(emJson))
	if err != nil {
		return err
	}

	err = dao.LiveDao.End(live.LiveId)
	return err
}

func (this *Control) chechLiveNow(liveId int) (*entity.Live, error) {
	live, err := dao.LiveDao.Now()
	if err != nil {
		return nil, err
	}

	if live == nil {
		return nil, entity.ErrorEmptyNowLive
	}

	if live.LiveId != liveId {
		return nil, entity.ErrorNotMatchNowLive
	}

	return live, nil
}
