package logkit

import (
	"log"
	"testing"
)

//检查/data/projlogs/logkit_test/下是否有相应的日志
func Test_Filelog(t *testing.T) {
	Init("logkit_test", LevelDebug)
	Info("test info")
	Error("test error")
	Debug("test debug")
	Action(Fields{"a": "a", "b": 1})
	Action("{\"a\":1}")
	Action("hello")
	Warn("test warn")
	log.Printf("it's a log")
	log.Printf("[D]it's a debug")
	log.Printf("[I]it's a info")
	log.Printf("[W]it's a warn")
	log.Printf("[E]it's a error")
	Exit()
}

func Test_Wait(t *testing.T) {
	Init("logkit_test", LevelDebug)
	Info("test wait")
	Error("test wait")
	Debug("test wait")
	Action(Fields{"a": "a", "b": 1})
	Action("{\"a\":1}")
	Warn("test wait")
}
