package entity

import (
	"errors"
	"time"
)

const (
	LiveStatusNoStart = 1
	LiveStatusStart   = 5
	LiveStatusFinish  = 10

	QuestionStatusUnshow = 0
	QuestionStatusShow   = 1
	QuestionStatusOff    = 2
)

var (
	ErrorLiveNotExist    = errors.New("live not exist")
	ErrorExistValidLive  = errors.New("exist valid live")
	ErrorMultiValidLive  = errors.New("multi valid live")
	ErrorEmptyNowLive    = errors.New("empty now live")
	ErrorLiveStatusWrong = errors.New("live status wrong")
	ErrorNotMatchNowLive = errors.New("not match now live")

	ErrorQuestionNotExist          = errors.New("question not exist")
	ErrorQuestionAlreadyUsed       = errors.New("question already used")
	ErrorExistQuestionGoing        = errors.New("exist question going")
	ErrorQuestionAlreadyShowAnswer = errors.New("question already show answer")
	ErrorQuestionNoShowAnswer      = errors.New("question no show answer")
	ErrorNotMatchNowQuestion       = errors.New("not match now question")
)

type Live struct {
	LiveId  int    `json:"liveId"`
	Title   string `json:"title"`
	Prize   int    `json:"prize"` // >= 1000
	Status  int    `json:"status"`
	PlanTs  string `json:"planTs"` //yyyy-MM-dd HH:mm:ss
	BeginTs string `json:"beginTs"`
	EndTs   string `json:"endTs"`

	Active    bool `json:"active,omitempty"`
	Countdown int  `json:"countdown,omitempty"`
}

type Question struct {
	LiveId        int    `json:"liveId"`
	QuestionId    int    `json:"questionId"`
	Desc          string `json:"desc"`
	Options       string `json:"options"`       //
	CorrectOption int    `json:"correctOption"` //0, 1, 2
	DisplayOrder  int    `json:"displayOrder"`
	AnswerTime    int    `json:"answerTime"` //答题可用时间
	Status        int    `json:"status"`
	Used          int    `json:"used"`
	AnswerShowed  int    `json:"answerShowed"`
}

//answer user
type CurrentQuestion struct {
	LiveId         int  `json: "liveId"`
	LiveStatus     int  `json: "liveStatus"`
	QuestionId     int  `json:"questionId"`
	QuestionStatus int  `json:"questionStatus"`
	CorrectOption  int  `json:"correctOption"`
	LastQuestionId int  `json:"lastQuestionId"`
	IsFinal        bool `json:"final"`
	AnswerShowed   int  `json:"questionAnswerShowed"`
	ShowTime       int  `json:"showTime"`
}

type QuestionMsg struct {
	AnswerTime   int    `json:"answerTime"`
	Desc         string `json:"desc"`
	DisplayOrder int    `json:"displayOrder"`
	LiveId       int    `json:"liveId"`
	Options      string `json:"options"` //"[\"哈哈\",\"呵呵\",\"嘻嘻\"]"
	QuestionId   int    `json:"questionId"`
	ShowTime     int    `json:"showTime"` //ms
	Status       int    `json:"status"`
	Ty           string `json:"type"` //showQuestion
}

func FormatQuestionMsg(q *Question) *QuestionMsg {
	qm := new(QuestionMsg)
	qm.AnswerTime = q.AnswerTime
	qm.Desc = q.Desc
	qm.DisplayOrder = q.DisplayOrder
	qm.LiveId = q.LiveId
	qm.Options = q.Options
	qm.QuestionId = q.QuestionId
	qm.ShowTime = int(time.Now().Unix()) * 1000
	qm.Status = q.Status
	qm.Ty = "showQuestion"

	return qm
}

type AnswerMsg struct {
	AnswerTime    int    `json:"answerTime"`
	CorrectOption int    `json:"correctOption"`
	Desc          string `json:"desc"`
	DisplayOrder  int    `json:"displayOrder"`
	LiveId        int    `json:"liveId"`
	Options       string `json:"options"` //"[\"哈哈\",\"呵呵\",\"嘻嘻\"]"
	QuestionId    int    `json:"questionId"`
	ShowTime      int    `json:"showTime"` //ms
	Stats         []int  `json:"stats"`    //[1,0,0]
	Status        int    `json:"status"`
	Ty            string `json:"type"` //showAnswer
}

func FormatAnswerMsg(q *Question, stats []int) *AnswerMsg {
	am := new(AnswerMsg)
	am.AnswerTime = q.AnswerTime
	am.CorrectOption = q.CorrectOption
	am.Desc = q.Desc
	am.DisplayOrder = q.DisplayOrder
	am.LiveId = q.LiveId
	am.Options = q.Options
	am.QuestionId = q.QuestionId
	am.ShowTime = int(time.Now().Unix()) * 1000
	am.Stats = stats
	am.Status = q.Status
	am.Ty = "showAnswer"

	return am
}

type WinnerMsg struct {
	LiveId      int       `json:"liveId"`
	ShowTime    int       `json:"showTime"` //ms
	TotalWinner int       `json:"totalWinner"`
	Ty          string    `json:"type"` //showWinner
	Wins        []*Winner `json:"wins"`
}

func FormatWinnerMsg(liveId, totalWinner int, wins []*Winner) *WinnerMsg {
	wm := new(WinnerMsg)
	wm.LiveId = liveId
	wm.ShowTime = int(time.Now().Unix()) * 1000
	wm.TotalWinner = totalWinner
	wm.Ty = "showWinner"
	wm.Wins = wins

	return wm
}

type Winner struct {
	Prize int       `json:"prize"`
	User  *UserInfo `json:"user"`
}

type UserInfo struct {
	UserId    int    `json:"userId"`
	Username  string `json:"username"`
	AvatarUrl string `json:"avatarUrl"`
}

type EndMsg struct {
	LiveId   int    `json:"liveId"`
	ShowTime int    `json:"showTime"` //ms
	Ty       string `json:"type"`     //liveEnd
}

func FormatEndMsg(liveId int) *EndMsg {
	em := new(EndMsg)
	em.LiveId = liveId
	em.ShowTime = int(time.Now().Unix()) * 1000
	em.Ty = "liveEnd"

	return em
}
