package config

import "github.com/BurntSushi/toml"

var gconf Config

type RedisConf struct {
	Addr     string `toml:"addr"`
	Password string `toml:"password"`
}

type Config struct {
	LiveSql       string    `toml:"live_sql"`
	QuestionSql   string    `toml:"question_sql"`
	RankSql       string    `toml:"rank_sql"`
	UserSql       string    `toml:"user_sql"`
	AnswerDomain  string    `toml:"answer_domain"`
	Port          int       `toml:"port"`
	MsgRedis      RedisConf `toml:"msg_redis"`
	QuestionRedis RedisConf `toml:"question_redis"`
	RankRedis     RedisConf `toml:"rank_redis"`
}

func Init(file string) error {
	_, err := toml.DecodeFile(file, &gconf)
	return err
}

func GetConfig() Config {
	return gconf
}
