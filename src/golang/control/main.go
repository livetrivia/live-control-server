package main

import (
	"flag"
	"golang/config"
	"golang/dao"
	"golang/service"

	"git.pandatv.com/panda-public/logkit"
)

var confFile = flag.String("conf_file", "./config.toml", "config file")

func main() {
	defer func() {
		if v := recover(); v != nil {
			logkit.Errorf("program panic:%v", v)
		}
		logkit.Exit()
	}()
	flag.Parse()
	config.Init(*confFile)
	logkit.Init("control", logkit.LevelInfo)

	var err error
	dao.AnswerDao, err = dao.NewAnswer()
	if err != nil {
		logkit.Errorf("init answer dao error:%s", err)
		return
	}
	dao.LiveDao, err = dao.NewLive()
	if err != nil {
		logkit.Errorf("init live dao error:%s", err)
		return
	}
	dao.MsgDao, err = dao.NewMsg()
	if err != nil {
		logkit.Errorf("init msg dao error:%s", err)
		return
	}
	dao.QuestionDao, err = dao.NewQuestion()
	if err != nil {
		logkit.Errorf("init question dao error:%s", err)
		return
	}
	dao.RankDao, err = dao.NewRank()
	if err != nil {
		logkit.Errorf("init rank dao error:%s", err)
		return
	}
	dao.UserDao, err = dao.NewUser()
	if err != nil {
		logkit.Errorf("init user dao error:%s", err)
		return
	}

	service.ControlService, err = service.NewControl()
	if err != nil {
		logkit.Errorf("init control service error:%s", err)
		return
	}
	service.LiveService, err = service.NewLive()
	if err != nil {
		logkit.Errorf("init live service error:%s", err)
		return
	}

	startRouter(config.GetConfig().Port)
}
