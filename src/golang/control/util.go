package main

import "golang/entity"

const (
	CodeSystemError = 500 + iota
	CodeExistValidLive
	CodeLiveNotExist
	CodeMultiValidLive
	CodeQuestionNotExist
)

const (
	CodeSuccess     = 0
	CodeParamsError = 401
)

const (
	SuccessMsg     = "success"
	SystemErrorMsg = "system error"
	ParamsErrorMsg = "params error"
)

func checkError(err error) (int, string) {
	switch err {
	case entity.ErrorExistValidLive:
		return CodeExistValidLive, err.Error()
	case entity.ErrorLiveNotExist:
		return CodeLiveNotExist, err.Error()
	case entity.ErrorMultiValidLive:
		return CodeMultiValidLive, err.Error()
	case entity.ErrorQuestionNotExist:
		return CodeQuestionNotExist, err.Error()
	default:
		return CodeSystemError, SystemErrorMsg
	}
}
