package main

import (
	"fmt"
	"golang/entity"
	"golang/service"
	"strconv"
	"time"

	"git.pandatv.com/panda-public/logkit"
	"github.com/gin-gonic/gin"
)

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func startRouter(port int) {
	if logkit.IsDebug() {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	gin.DefaultWriter = logkit.NewLogWriter(logkit.LevelInfo)
	gin.DefaultErrorWriter = logkit.NewLogWriter(logkit.LevelError)
	r := gin.New()
	r.Use(gin.Recovery())
	if logkit.IsDebug() {
		r.Use(gin.Logger())
	}
	live := r.Group("/live")
	{
		live.POST("/create", liveCreate)
		live.POST("/copyCreate", liveCopyCreate)
		live.GET("/now", liveNow)
		live.GET("/list", liveList)
		live.GET("/one", liveOne)
		live.POST("/update", liveUpdate)
		live.POST("/start", liveStart)
		live.POST("/end", liveEnd)
		live.GET("/showQuestion", liveShowQuestion)
		live.GET("/showAnswer", liveShowAnswer)
		live.GET("/showWinner", liveShowWinner)
	}

	r.Run(fmt.Sprintf(":%d", port))
}

func liveCreate(c *gin.Context) {
	title := c.PostForm("title")
	prize := c.PostForm("prize")
	planTs := c.PostForm("planTs")

	iprize, _ := strconv.Atoi(prize)
	tplanTs, err := time.Parse("2006-01-02 15:04:05", planTs)
	if title == "" || iprize <= 0 || err != nil || tplanTs.Before(time.Now()) {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	liveId, err := service.LiveService.Create(title, iprize, planTs)
	if err != nil {
		logkit.Errorf("live create, title:%s, prize:%d, planTs:%s, error:%s", title, iprize, planTs, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: liveId,
	})
}

func liveCopyCreate(c *gin.Context) {
	liveId := c.PostForm("liveId")

	iliveId, _ := strconv.Atoi(liveId)
	if iliveId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	newLiveId, err := service.LiveService.CopyCreate(iliveId)
	if err != nil {
		logkit.Errorf("live copy create, liveId:%d, error:%s", iliveId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: newLiveId,
	})
}

func liveNow(c *gin.Context) {
	live, err := service.LiveService.Now()
	if err != nil {
		logkit.Errorf("live now, error:%s", err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: live,
	})
}

type ListResp struct {
	Total int            `json:"total"`
	List  []*entity.Live `json:"list"`
}

func liveList(c *gin.Context) {
	pageno := c.Query("pageno")
	pagesize := c.Query("pagesize")

	ipageno, _ := strconv.Atoi(pageno)
	ipagesize, _ := strconv.Atoi(pagesize)
	if ipageno <= 0 || ipagesize <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	lives, total, err := service.LiveService.List(ipageno, ipagesize)
	if err != nil {
		logkit.Errorf("live list, pageno:%d, pagesize:%d error:%s", ipageno, ipagesize, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: ListResp{
			Total: total,
			List:  lives,
		},
	})
}

func liveOne(c *gin.Context) {
	liveId := c.Query("liveId")

	iliveId, _ := strconv.Atoi(liveId)
	if iliveId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	live, err := service.LiveService.Get(iliveId)
	if err != nil {
		logkit.Errorf("live one, liveId:%d error:%s", iliveId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: live,
	})
}

func liveUpdate(c *gin.Context) {
	liveId := c.PostForm("liveId")
	title := c.PostForm("title")
	prize := c.PostForm("prize")
	planTs := c.PostForm("planTs")

	iliveId, _ := strconv.Atoi(liveId)
	iprize, _ := strconv.Atoi(prize)
	tplanTs, err := time.Parse("2006-01-02 15:04:05", planTs)
	if iliveId <= 0 || title == "" || iprize <= 0 || err != nil || tplanTs.Before(time.Now()) {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	live, err := service.LiveService.Update(iliveId, title, iprize, planTs)
	if err != nil {
		logkit.Errorf("live update, liveId:%d, title:%s, prize:%d, planTs:%s, error:%s", iliveId, title, iprize, planTs, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
		Data: live,
	})
}

func liveStart(c *gin.Context) {
	liveId := c.Query("liveId")
	iliveId, _ := strconv.Atoi(liveId)
	if iliveId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	err := service.ControlService.Start(iliveId)
	if err != nil {
		logkit.Errorf("live start, liveId:%d, error:%s", iliveId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
	})
}

func liveEnd(c *gin.Context) {
	liveId := c.Query("liveId")
	iliveId, _ := strconv.Atoi(liveId)
	if iliveId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	err := service.ControlService.End(iliveId)
	if err != nil {
		logkit.Errorf("live end, liveId:%d, error:%s", iliveId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
	})
}

func liveShowQuestion(c *gin.Context) {
	liveId := c.Query("liveId")
	questionId := c.Query("questionId")
	iliveId, _ := strconv.Atoi(liveId)
	iquestionId, _ := strconv.Atoi(questionId)
	if iliveId <= 0 || iquestionId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	err := service.ControlService.ShowQuestion(iliveId, iquestionId)
	if err != nil {
		logkit.Errorf("live show question, liveId:%d, questionId:%d, error:%s", iliveId, iquestionId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
	})
}

func liveShowAnswer(c *gin.Context) {
	liveId := c.Query("liveId")
	questionId := c.Query("questionId")
	iliveId, _ := strconv.Atoi(liveId)
	iquestionId, _ := strconv.Atoi(questionId)
	if iliveId <= 0 || iquestionId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	err := service.ControlService.ShowAnswer(iliveId, iquestionId)
	if err != nil {
		logkit.Errorf("live show answer, liveId:%d, questionId:%d, error:%s", iliveId, iquestionId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
	})
}

func liveShowWinner(c *gin.Context) {
	liveId := c.Query("liveId")
	questionId := c.Query("questionId")
	iliveId, _ := strconv.Atoi(liveId)
	iquestionId, _ := strconv.Atoi(questionId)
	if iliveId <= 0 || iquestionId <= 0 {
		c.JSON(200, &Response{
			Code: CodeParamsError,
			Msg:  ParamsErrorMsg,
		})
		return
	}

	err := service.ControlService.ShowWinner(iliveId, iquestionId)
	if err != nil {
		logkit.Errorf("live show winner, liveId:%d, questionId:%d, error:%s", iliveId, iquestionId, err)
		code, msg := checkError(err)
		c.JSON(200, &Response{
			Code: code,
			Msg:  msg,
		})
		return
	}

	c.JSON(200, &Response{
		Code: CodeSuccess,
		Msg:  SuccessMsg,
	})
}
