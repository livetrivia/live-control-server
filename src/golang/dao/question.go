package dao

import (
	"database/sql"
	"encoding/json"
	"golang/config"
	"golang/entity"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

const (
	currentQuestionKey = "current-question"
)

type Question struct {
	dbcli *sql.DB
	rdcli *redis.Client
}

func NewQuestion() (*Question, error) {
	dbcli, err := sql.Open("mysql", config.GetConfig().QuestionSql)
	if err != nil {
		return nil, err
	}

	rdcli := redis.NewClient(&redis.Options{
		Addr:         config.GetConfig().QuestionRedis.Addr,
		Password:     config.GetConfig().QuestionRedis.Password,
		DialTimeout:  time.Second,
		ReadTimeout:  500 * time.Millisecond,
		WriteTimeout: 500 * time.Millisecond,
	})

	return &Question{dbcli: dbcli, rdcli: rdcli}, nil
}

func (this *Question) GetQuestionByOrder(liveId int, questionOrder int) (*entity.Question, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT * FROM question WHERE liveId=? and displayOrder=?", liveId, questionOrder)
	if err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return changeMapToQuestion(rows[0]), nil
}

func (this *Question) GetQuestionById(questionId int) (*entity.Question, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT * FROM question WHERE questionId=?", questionId)
	if err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return changeMapToQuestion(rows[0]), nil
}

func (this *Question) SetQuestionStatus(questionId int, status int) error {
	_, _, err := SqlExec(this.dbcli, "UPDATE question SET status=? WHERE questionId=?", status, questionId)
	return err
}

func (this *Question) SetQuestionShowAnswerAndUsed(questionId int) error {
	_, _, err := SqlExec(this.dbcli, "UPDATE question SET answerShowed=1, used=1 WHERE questionId=?", questionId)
	return err
}

func changeMapToQuestion(m map[string]string) *entity.Question {
	q := new(entity.Question)
	q.AnswerShowed, _ = strconv.Atoi(m["answerShowed"])
	q.AnswerTime, _ = strconv.Atoi(m["answerTime"])
	q.CorrectOption, _ = strconv.Atoi(m["correctOption"])
	q.Desc, _ = m["desc"]
	q.DisplayOrder, _ = strconv.Atoi(m["displayOrder"])
	q.LiveId, _ = strconv.Atoi(m["liveId"])
	q.Options = m["options"]
	q.QuestionId, _ = strconv.Atoi(m["questionId"])
	q.Status, _ = strconv.Atoi(m["status"])
	q.Used, _ = strconv.Atoi(m["used"])

	return q
}

func (this *Question) SetCurrentQuestion(question *entity.CurrentQuestion) error {
	questionJson, _ := json.Marshal(question)
	_, err := this.rdcli.Set(currentQuestionKey, string(questionJson), 0).Result()
	return err
}

func (this *Question) GetCurrentQuestion() (*entity.CurrentQuestion, error) {
	val, err := this.rdcli.Get(currentQuestionKey).Result()
	if err != nil {
		return nil, err
	}

	if val == "" {
		return nil, nil
	}

	var question entity.CurrentQuestion
	err = json.Unmarshal([]byte(val), &question)
	if err != nil {
		return nil, err
	}
	return &question, nil
}

func (this *Question) SetCurrentQuestionStatus(questionId, status int) error {
	question, err := this.GetCurrentQuestion()
	if err != nil {
		return err
	}
	if question == nil {
		return nil
	}
	if question.QuestionId != questionId {
		return nil
	}
	question.QuestionStatus = status

	return this.SetCurrentQuestion(question)
}

func (this *Question) DelCurrentQuestion() error {
	_, err := this.rdcli.Del(currentQuestionKey).Result()
	return err
}
