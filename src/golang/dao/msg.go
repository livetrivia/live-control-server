package dao

import (
	"golang/config"
	"time"

	"github.com/go-redis/redis"
)

const (
	msgKey = "current_push_question"
)

type Msg struct {
	rdcli *redis.Client
}

func NewMsg() (*Msg, error) {
	rdcli := redis.NewClient(&redis.Options{
		Addr:         config.GetConfig().MsgRedis.Addr,
		Password:     config.GetConfig().MsgRedis.Password,
		DialTimeout:  time.Second,
		ReadTimeout:  500 * time.Millisecond,
		WriteTimeout: 500 * time.Millisecond,
	})

	return &Msg{rdcli: rdcli}, nil
}

func (this *Msg) Push(content string) error {
	_, err := this.rdcli.Set(msgKey, content, 0).Result()
	return err
}
