package dao

import (
	"database/sql"
	"fmt"
	"golang/config"
	"strconv"
	"strings"
	"time"

	"git.pandatv.com/panda-public/logkit"

	"github.com/go-redis/redis"
)

const (
	totalRankKey = "total-rank-key"
	weekRankKey  = "weekly-rank-key-%s"
)

type Rank struct {
	dbcli *sql.DB
	rdcli *redis.Client
}

func NewRank() (*Rank, error) {
	dbcli, err := sql.Open("mysql", config.GetConfig().RankSql)
	if err != nil {
		return nil, err
	}

	rdcli := redis.NewClient(&redis.Options{
		Addr:         config.GetConfig().RankRedis.Addr,
		Password:     config.GetConfig().RankRedis.Password,
		DialTimeout:  time.Second,
		ReadTimeout:  500 * time.Millisecond,
		WriteTimeout: 500 * time.Millisecond,
	})

	return &Rank{dbcli: dbcli, rdcli: rdcli}, nil
}

func (this *Rank) InsertWinner(liveId int, winners []int, prize int) error {
	if len(winners) == 0 {
		return nil
	}
	args := make([]string, len(winners))
	for i, winner := range winners {
		args[i] = fmt.Sprintf("(%d, %d, %d)", liveId, winner, prize)
	}
	argsStr := strings.Join(args, ",")
	queryStr := fmt.Sprintf("INSERT INTO win(liveId, userId, prize) VALUES%s", argsStr)

	_, _, err := SqlExec(this.dbcli, queryStr)
	return err
}

func (this *Rank) CheckInsert(liveId int) (bool, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT count(*) as c FROM win WHERE liveId=?", liveId)
	if err != nil {
		return false, err
	}

	count, _ := strconv.Atoi(rows[0]["c"])
	if count > 0 {
		return true, nil
	}
	return false, nil
}

func (this *Rank) UpdateRank() error {
	rows, err := SqlSelect(this.dbcli, "SELECT userId, sum(prize) as prize FROM win GROUP BY userId ORDER BY prize DESC limit 100")
	if err != nil {
		return err
	}

	for _, row := range rows {
		userId := row["userId"]
		prize, _ := strconv.ParseFloat(row["prize"], 64)
		_, err = this.rdcli.ZAdd(totalRankKey, redis.Z{Score: prize, Member: userId}).Result()
		if err != nil {
			logkit.Errorf("total rank, add userId:%s, prize:%f error:%s", userId, prize, err)
		}
	}

	sevenDayAgo := time.Now().Add(time.Hour * 24 * -7)
	rows, err = SqlSelect(this.dbcli, "SELECT userId, sum(prize) as prize FROM win WHERE created > ? GROUP BY userId ORDER BY prize DESC limit 100", sevenDayAgo.Format("2006-01-02 15:04:05"))
	if err != nil {
		return err
	}

	weekRankKeyCur := fmt.Sprintf(weekRankKey, sevenDayAgo.Format("2006-01-02"))

	for _, row := range rows {
		userId := row["userId"]
		prize, _ := strconv.ParseFloat(row["prize"], 64)
		_, err = this.rdcli.ZAdd(weekRankKeyCur, redis.Z{Score: prize, Member: userId}).Result()
		if err != nil {
			logkit.Errorf("week rank %s, add userId:%s, prize:%f error:%s", weekRankKeyCur, userId, prize, err)
		}
	}

	return nil
}
