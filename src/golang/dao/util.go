package dao

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func SqlExec(db *sql.DB, sqlstr string, args ...interface{}) (int64, int64, error) {
	result, err := db.Exec(sqlstr, args...)
	if err != nil {
		return 0, 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, 0, err
	}

	num, err := result.RowsAffected()
	if err != nil {
		return 0, 0, err
	}

	return id, num, nil
}

func SqlSelect(db *sql.DB, sqlstr string, args ...interface{}) ([]map[string]string, error) {
	rows, err := db.Query(sqlstr, args...)

	if err != nil {
		return nil, err
	}

	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	values := make([]sql.RawBytes, len(columns))
	scanArgs := make([]interface{}, len(values))
	var rets = make([]map[string]string, 0)

	for i := range values {
		scanArgs[i] = &values[i]
	}

	for rows.Next() {
		err = rows.Scan(scanArgs...)
		if err != nil {
			return nil, err
		}

		var ret = make(map[string]string)
		var value string
		for i, col := range values {
			if col == nil {
				value = ""
			} else {
				value = string(col)
			}
			ret[columns[i]] = value
		}

		rets = append(rets, ret)
	}

	return rets, err
}

func HttpGetJson(cli *http.Client, reqUrl string, v interface{}) error {
	resp, err := cli.Get(reqUrl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, v)
	if err != nil {
		return err
	}

	return nil
}
