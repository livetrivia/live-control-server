package dao

import (
	"bytes"
	"database/sql"
	"golang/config"
	"golang/entity"
	"strconv"
	"strings"
	"sync"
	"time"

	"git.pandatv.com/panda-public/logkit"
	_ "github.com/go-sql-driver/mysql"
)

type Live struct {
	rwlock      *sync.RWMutex
	currentLive *entity.Live
	dbcli       *sql.DB
}

func NewLive() (*Live, error) {
	db, err := sql.Open("mysql", config.GetConfig().LiveSql)
	if err != nil {
		return nil, err
	}

	l := &Live{dbcli: db, rwlock: new(sync.RWMutex)}
	go l.updateNow()

	return l, nil
}

func (this *Live) updateNow() {
	tc := time.NewTicker(time.Millisecond * 500)

	for range tc.C {
		rows, err := SqlSelect(this.dbcli, "SELECT * FROM live WHERE status = ?", entity.LiveStatusStart)
		if err != nil {
			logkit.Errorf("select now live error:%s", err)
			continue
		}

		if len(rows) > 1 {
			logkit.Errorf("select now live error:%s", entity.ErrorMultiValidLive)
			continue
		}

		if len(rows) == 1 {
			this.rwlock.Lock()
			this.currentLive = changeMapToLive(rows[0])
			this.rwlock.Unlock()
			continue
		}

		rows, err = SqlSelect(this.dbcli, `SELECT * FROM live 
			WHERE status=? and planTs >= now() ORDER BY planTs ASC limit 1`, entity.LiveStatusNoStart)
		if err != nil {
			logkit.Errorf("select now live error:%s", err)
			continue
		}

		this.rwlock.Lock()
		if len(rows) == 0 {
			this.currentLive = nil
		} else {
			this.currentLive = changeMapToLive(rows[0])
		}
		this.rwlock.Unlock()
	}
}

func (this *Live) Create(title string, prize int, planTs string) (*entity.Live, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT * FROM live WHERE status=?", entity.LiveStatusStart)
	if err != nil {
		return nil, err
	}

	if len(rows) > 0 {
		return nil, entity.ErrorExistValidLive
	}

	liveId, _, err := SqlExec(this.dbcli,
		"INSERT INTO live(title, status, prize, planTs) VALUES(?, ?, ?, ?)",
		title, entity.LiveStatusNoStart, prize, planTs)

	if err != nil {
		return nil, err
	}

	return this.Get(int(liveId))
}

func (this *Live) Get(liveId int) (*entity.Live, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT * FROM live WHERE liveId = ?", liveId)
	if err != nil {
		return nil, err
	}

	if len(rows) == 0 {
		return nil, nil
	}

	return changeMapToLive(rows[0]), nil
}

func (this *Live) CopyCreate(liveId int) (*entity.Live, error) {
	el, err := this.Get(liveId)
	if err != nil {
		return nil, err
	}

	if el == nil {
		return nil, entity.ErrorLiveNotExist
	}

	return this.Create(el.Title, el.Prize, el.PlanTs)
}

func (this *Live) Now() (*entity.Live, error) {
	this.rwlock.RLock()
	defer this.rwlock.RUnlock()
	return this.currentLive, nil
}

//pageno 1 is first page
func (this *Live) List(pageno, pagesize int) ([]*entity.Live, int, error) {
	total, err := this.count()
	if err != nil {
		return nil, 0, err
	}

	rows, err := SqlSelect(this.dbcli, "SELECT * FROM live ORDER BY liveId DESC LIMIT ?, ?", (pageno-1)*pagesize, pagesize)
	if err != nil {
		return nil, 0, err
	}

	els := make([]*entity.Live, len(rows))
	for i, row := range rows {
		els[i] = changeMapToLive(row)
	}

	return els, total, nil
}

func (this *Live) count() (int, error) {
	rows, err := SqlSelect(this.dbcli, "SELECT count(*) as c FROM live")
	if err != nil {
		return 0, err
	}

	count, _ := strconv.Atoi(rows[0]["c"])
	return count, nil
}

func (this *Live) Update(liveId int, title string, prize int, planTs string) (*entity.Live, error) {
	var buf bytes.Buffer
	setParts := make([]string, 0)
	setArgs := make([]interface{}, 0)

	buf.WriteString("UPDATE live SET ")
	if title != "" {
		setParts = append(setParts, "title=?")
		setArgs = append(setArgs, title)
	}
	if prize > 0 {
		setParts = append(setParts, "prize=?")
		setArgs = append(setArgs, prize)
	}
	if planTs != "" {
		setParts = append(setParts, "planTs=?")
		setArgs = append(setArgs, planTs)
	}
	setPartsString := strings.Join(setParts, ",")
	buf.WriteString(setPartsString)
	buf.WriteString(" where liveId=?")
	setArgs = append(setArgs, liveId)

	_, num, err := SqlExec(this.dbcli, buf.String(), setArgs...)
	if err != nil {
		return nil, err
	}

	if num != 1 {
		return nil, entity.ErrorLiveNotExist
	}

	return this.Get(liveId)
}

func (this *Live) Start(liveId int) error {
	_, num, err := SqlExec(this.dbcli, "UPDATE live SET status=? WHERE liveId=?", entity.LiveStatusStart, liveId, entity.LiveStatusFinish)
	if err != nil {
		return err
	}

	if num != 1 {
		return entity.ErrorLiveNotExist
	}

	return nil
}

func (this *Live) End(liveId int) error {
	_, num, err := SqlExec(this.dbcli, "UPDATE live SET status=?, endTs=? WHERE liveId=?", entity.LiveStatusStart, time.Now().String(), liveId)
	if err != nil {
		return err
	}

	if num != 1 {
		return entity.ErrorLiveNotExist
	}

	return nil
}

func changeMapToLive(m map[string]string) *entity.Live {
	el := new(entity.Live)
	el.LiveId, _ = strconv.Atoi(m["liveId"])
	el.Title = m["title"]
	el.Prize, _ = strconv.Atoi(m["prize"])
	el.Status, _ = strconv.Atoi(m["status"])
	el.PlanTs = m["planTs"]
	el.BeginTs = m["beginTs"]
	el.EndTs = m["endTs"]

	return el
}
