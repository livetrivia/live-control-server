package dao

import "golang/entity"

type ILiveDao interface {
	Create(title string, prize int, planTs string) (*entity.Live, error)
	CopyCreate(liveId int) (*entity.Live, error)
	Get(liveId int) (*entity.Live, error)
	Now() (*entity.Live, error)
	List(pageno, pagesize int) ([]*entity.Live, int, error)
	Update(liveId int, title string, prize int, planTs string) (*entity.Live, error)

	Start(liveId int) error
	End(liveId int) error
}

var LiveDao ILiveDao

type IAnswerDao interface {
	GetStatistic(liveId, questionId int) ([]int, error)
	GetWinner(liveId int) ([]int, error)
}

var AnswerDao IAnswerDao

type IMsgDao interface {
	Push(content string) error
}

var MsgDao IMsgDao

type IQuestionDao interface {
	GetQuestionByOrder(liveId int, questionOrder int) (*entity.Question, error)
	GetQuestionById(questionId int) (*entity.Question, error)
	SetQuestionStatus(questionId int, status int) error
	SetQuestionShowAnswerAndUsed(questionId int) error
	SetCurrentQuestion(question *entity.CurrentQuestion) error
	GetCurrentQuestion() (*entity.CurrentQuestion, error)
	SetCurrentQuestionStatus(questionId, status int) error
	DelCurrentQuestion() error
}

var QuestionDao IQuestionDao

type IRankDao interface {
	InsertWinner(liveId int, winners []int, prize int) error
	UpdateRank() error
	CheckInsert(liveId int) (bool, error)
}

var RankDao IRankDao

type IUserDao interface {
	ListUser(userIds []int) ([]*entity.UserInfo, error)
	UpdatePrize(userIds []int, prize int) error
}

var UserDao IUserDao
