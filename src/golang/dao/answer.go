package dao

import (
	"fmt"
	"golang/config"
	"net/http"
	"time"
)

type Answer struct {
	httpcli *http.Client
	domain  string
}

func NewAnswer() (*Answer, error) {
	cli := &http.Client{
		Timeout: time.Second,
	}
	return &Answer{httpcli: cli, domain: config.GetConfig().AnswerDomain}, nil
}

type AnswerStatResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		Option []int `json:"option"`
	} `json:"data"`
}

func (this *Answer) GetStatistic(liveId, questionId int) ([]int, error) {
	reqUrl := fmt.Sprintf("http://%s:8360/answer/statistic?liveId=%d&questionId=%d",
		this.domain, liveId, questionId)

	var asp AnswerStatResp
	err := HttpGetJson(this.httpcli, reqUrl, &asp)
	if err != nil {
		return nil, err
	}

	return asp.Data.Option, nil
}

type AnswerWinnerResp struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data struct {
		Winner []int `json:"winner"`
	} `json:"data"`
}

func (this *Answer) GetWinner(liveId int) ([]int, error) {
	reqUrl := fmt.Sprintf("http://%s:8360/answer/winner?liveId=%d",
		this.domain, liveId)

	var awp AnswerWinnerResp
	err := HttpGetJson(this.httpcli, reqUrl, &awp)
	if err != nil {
		return nil, err
	}

	return awp.Data.Winner, nil
}
