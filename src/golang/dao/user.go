package dao

import (
	"database/sql"
	"fmt"
	"golang/config"
	"golang/entity"
	"strconv"
	"strings"
)

type User struct {
	dbcli *sql.DB
}

func NewUser() (*User, error) {
	db, err := sql.Open("mysql", config.GetConfig().UserSql)
	if err != nil {
		return nil, err
	}

	return &User{dbcli: db}, nil
}

func (this *User) ListUser(userIds []int) ([]*entity.UserInfo, error) {
	userInfos := make([]*entity.UserInfo, len(userIds))
	if len(userIds) == 0 {
		return userInfos, nil
	}

	userIdsStrSclie := make([]string, len(userIds))
	for i, userId := range userIds {
		userIdsStrSclie[i] = strconv.Itoa(userId)
	}

	userIdsStr := strings.Join(userIdsStrSclie, ",")
	queryStr := fmt.Sprintf("SELECT username, userId, avatarUrl FROM user WHERE userId in (%s)", userIdsStr)

	rows, err := SqlSelect(this.dbcli, queryStr)
	if err != nil {
		return nil, err
	}

	for i, row := range rows {
		userInfos[i] = changeMapToUser(row)
	}

	return userInfos, nil
}

func (this *User) UpdatePrize(userIds []int, prize int) error {
	if len(userIds) == 0 {
		return nil
	}

	userIdsStrSclie := make([]string, len(userIds))
	for i, userId := range userIds {
		userIdsStrSclie[i] = strconv.Itoa(userId)
	}

	userIdsStr := strings.Join(userIdsStrSclie, ",")
	queryStr := fmt.Sprintf("UPDATE user SET balance=balance+%d, income=income+%d WHERE userId in (%s)", prize, prize, userIdsStr)

	_, _, err := SqlExec(this.dbcli, queryStr)
	return err
}

func changeMapToUser(m map[string]string) *entity.UserInfo {
	el := new(entity.UserInfo)
	el.Username = m["username"]
	el.UserId, _ = strconv.Atoi(m["userId"])
	el.AvatarUrl = m["avatarUrl"]

	return el
}
